from __future__ import absolute_import, division, print_function, unicode_literals

__metaclass__ = type

import socket
import threading
from time import sleep
from collections import deque

import editor.logging as logging
from editor.constants import *
from editor.exceptions import *
from editor.communication import CommunicationThread

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

# Configuration
LOG_LEVEL = logging.DEBUG
LISTENING_ADDRESS = 'localhost'
LISTENING_PORT = 9898


class DocumentStorage:
    """
    A basic in-memory document storage class for the collaborative editor

    Has locks for multithreaded environment
    """

    def __init__(self):
        self.lock = threading.Lock()
        self.storage = []
        self.idTable = {}

    def addDocument(self, name, owner):
        self.lock.acquire()
        self.storage.append({
            'name': name,
            'editors': [owner],
            'buffer': Gtk.TextBuffer(),
            'modifications': [],
            'version': 1,
            'lock': threading.Lock()
        })
        self.idTable[name] = len(self.storage) - 1
        self.lock.release()

    def documentExists(self, name):
        with self.lock:
            return name in self.idTable

    def getDocument(self, docName):
        if docName not in self.idTable:
            raise DocumentNotFoundError('Document is not found!')

        return self.storage[self.idTable[docName]]

    def checkPermission(self, name, docName):
        return name in self.getDocument(docName)['editors']

    def addEditor(self, requesterName, docName, newEditor):
        """
        Add a new editor
        :param requesterName:
        :param docName:
        :param newEditor:
        """
        self.getDocument(docName)['editors'].append(newEditor)

    def documentInsertText(self, docName, offset, text):
        doc = self.getDocument(docName)

        with doc['lock']:
            iter = doc['buffer'].get_iter_at_offset(offset)
            doc['buffer'].insert(iter, text)
            doc['modifications'].append((ACTION_INSERT, offset, text))

    def documentDeleteText(self, docName, start, end):
        doc = self.getDocument(docName)
        buf = doc['buffer']

        with doc['lock']:
            startIter = buf.get_iter_at_offset(start)
            endIter = buf.get_iter_at_offset(end)
            deletedText = buf.get_slice(startIter, endIter, False)
            buf.delete(startIter, endIter)
            doc['modifications'].append((ACTION_DELETE, start, end, deletedText))


class CollaborationServer:
    """
    Main thread handling new clients and storing information about them
    """

    def __init__(self):
        self.listeningSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listeningSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.listeningSocket.bind((LISTENING_ADDRESS, LISTENING_PORT))
        self.listeningSocket.listen(3)

        self.documentStorage = DocumentStorage()
        self.clients = []

    def run(self):
        try:
            while True:
                (clientSocket, address) = self.listeningSocket.accept()

                thread = SessionThread(clientSocket, self)
                self.clients.append(thread)
                thread.start()

        except KeyboardInterrupt:
            log.info('Exiting server!')
            for session in self.clients:
                session.stop.set()
            self.listeningSocket.close()


class SessionThread(CommunicationThread):
    """
    Thread for communicating with the client
    """

    def __init__(self, sock, serverObject):
        """
        Initiates a new client session handler
        """
        CommunicationThread.__init__(self)

        self.requestHandlerTable = {
            REQ_QUIT: self.quitRequestHandler,
            REQ_SET_NAME: self.setNameRequestHandler,
            REQ_NEW_DOCUMENT: self.newDocumentRequestHandler,
            REQ_INSERT_TEXT: self.insertTextRequestHandler,
            REQ_DELETE_TEXT: self.deleteTextRequestHandler
        }

        self.server = server
        self.docs = server.documentStorage
        self.socket = sock
        self.address, self.port = self.socket.getpeername()

        self.clientName = None
        self.openedDocument = None

    def run(self):
        """
        Receiving and processing client's requests
        """

        log.info('Client connected: %s:%i', self.address, self.port)
        CommunicationThread.run(self)
        log.info('Thread for client %s:%i stopping.', self.address, self.port)
        self.socket.close()

    def quitRequestHandler(self, params):
        self.stop.set()

    def setNameRequestHandler(self, params):
        self.clientName = params[0]
        log.debug('Client name set to %s', self.clientName)

    def newDocumentRequestHandler(self, params):
        try:
            docName = params[0]
        except IndexError:
            log.error('[client %s] New document request missing filename', self.clientName)
            return

        if self.docs.documentExists(docName):
            self.send(REQ_NEW_DOCUMENT, [REQ_FALSE])
            log.debug('[client %s] New document name not unique: %s', self.clientName, docName)
        else:
            self.docs.addDocument(docName, self.clientName)
            self.openedDocument = docName
            self.send(REQ_NEW_DOCUMENT, [REQ_TRUE])
            log.debug('[client %s] New document created: %s', self.clientName, docName)

    def addEditorRequestHandler(self, params):
        try:
            docName, newEditorName = params
            self.checkPermission(docName, 'add editor')
            # TODO ...
        except Exception as e:
            log.error(e)
            return

    def insertTextRequestHandler(self, params):
        offset, text = params
        self.checkPermission(self.openedDocument, 'insert text')
        self.docs.documentInsertText(self.openedDocument, int(offset), text)

    def deleteTextRequestHandler(self, params):
        start, end = params
        self.checkPermission(self.openedDocument, 'delete text')
        self.docs.documentDeleteText(self.openedDocument, int(start), int(end))

    def checkPermission(self, docName, action='access'):
        if not self.docs.checkPermission(self.clientName, docName):
            raise PermissionDeniedError("Action denied, user: %s, doc: %s, action: %s" % (self.clientName, docName, action))


if __name__ == '__main__':
    logging.setupCustomLogger(LOG_LEVEL)
    log = logging.getCustomLogger()

    server = CollaborationServer()
    server.run()
