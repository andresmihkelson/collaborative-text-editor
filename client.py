from __future__ import absolute_import, division, print_function, unicode_literals
__metaclass__ = type

import threading
import socket
from collections import deque
from time import sleep

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import editor.logging as logging
from editor.constants import *
from editor.exceptions import *
from editor.dialogs import *
from editor.communication import CommunicationThread


# Configuration
LOG_LEVEL = logging.DEBUG
SERVER_ADDRESS = 'localhost'
SERVER_PORT = 9898


class TextEditorApp(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

        self.client = CollaborationClient()
        self.window = TextEditorWindow(self.client)

        self.connect('activate', self.startupCommand)

    def startupCommand(self, app):
        app.add_window(self.window)
        self.client.start()

        if not self.startupRoutine():
            self.window.close()

    def startupRoutine(self, *args):
        """
        Asking for the name and registering on the server
        """
        dialog = TextEntryDialog(self.window, 'Hello', 'Please enter Your username:')

        while True:
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                name = dialog.getValue()
                if not name:
                    errorDialog = Gtk.MessageDialog(
                        dialog, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                        'Name cannot be empty!'
                    )
                    errorDialog.run()
                    errorDialog.destroy()
                    continue

                self.client.sendSetNameRequest(dialog.getValue())
            else:
                self.client.sendQuitRequest()
            break

        dialog.destroy()
        return response == Gtk.ResponseType.OK


class TextEditorWindow(Gtk.ApplicationWindow):

    def __init__(self, collaborationClient):
        """

        :param collaborationClient: CollaborationClient
        """
        self.client = collaborationClient

        Gtk.ApplicationWindow.__init__(self, title='Collaborative text editor')
        self.connect('delete-event', self.client.sendQuitRequest)

        self.set_border_width(10)

        self.createLayout()
        self.addButtons()
        self.addTextView()

        self.show_all()

    def createLayout(self):
        """
        Creates the GUI layout
        """
        self.verticalBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.add(self.verticalBox)

        self.buttonBox = Gtk.Box(spacing=10)
        self.verticalBox.pack_start(self.buttonBox, False, True, 0)

    def addButtons(self):
        """
        Adds all the command buttons and connects them to handlers
        """
        newDocumentButton = Gtk.Button.new_with_label('New document')
        newDocumentButton.connect('clicked', self.newDocumentCommand)
        self.buttonBox.pack_start(newDocumentButton, False, False, 0)

        existingDocumentButton = Gtk.Button.new_with_label('Open existing')
        existingDocumentButton.connect('clicked', self.documentListCommand)
        self.buttonBox.pack_start(existingDocumentButton, False, False, 0)

    def addTextView(self):
        """
        Sets up the text editor component inside a scrollable box
        """
        scrollingContainer = Gtk.ScrolledWindow()
        scrollingContainer.set_hexpand(True)
        scrollingContainer.set_vexpand(True)
        self.verticalBox.pack_start(scrollingContainer, True, True, 0)

        self.textView = Gtk.TextView()
        scrollingContainer.add(self.textView)

        self.textView.set_editable(False)
        self.textView.set_cursor_visible(False)

        self.client.attachTextView(self.textView)

    def setupBuffer(self, text=''):
        buffer = Gtk.TextBuffer()
        buffer.connect('insert-text', self.client.insertTextRequest)
        buffer.connect('delete-range', self.client.deleteTextRequest)

        self.textView.set_buffer(buffer)
        self.textView.set_editable(True)
        self.textView.set_cursor_visible(True)

    def newDocumentCommand(self, *args):
        """
        Shows the dialog for entering a new documents name and sends the corresponding command to the server
        """
        dialog = TextEntryDialog(self, 'New document', 'Enter the name for document being created:')

        while True:
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                name = dialog.getValue()

                errMsg = None
                if not name:
                    errMsg = 'Name cannot be empty!'
                elif not self.client.sendNewDocumentRequest(name):
                    errMsg = 'Document with this name already exists'

                if errMsg is not None:
                    errorDialog = Gtk.MessageDialog(dialog, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, errMsg)
                    errorDialog.run()
                    errorDialog.destroy()
                    continue

                self.setupBuffer()

            break

        dialog.destroy()

    def documentListCommand(self, *args):
        """
        Shows the existing document list from the server and let's the user pick one
        """
        pass


class CollaborationClient(CommunicationThread):

    def __init__(self):
        CommunicationThread.__init__(self)

        self.requestHandlerTable = {
            REQ_NEW_DOCUMENT: self.newDocumentRequestHandler,
            REQ_LIST_DOCUMENTS: self.documentListRequestHandler,
            REQ_LIST_CLIENTS: self.clientListRequestHandler,
            REQ_LIST_ACCESS: self.accessListRequestHandler,
            REQ_INSERT_TEXT: self.insertTextRequestHandler,
            REQ_DELETE_TEXT: self.deleteTextRequestHandler
        }

        self.events = {
            'requestComplete': threading.Event()
        }
        self.textView = None
        self.clientName = None

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((SERVER_ADDRESS, SERVER_PORT))

    def addEventObject(self, name):
        self.events[name] = threading.Event()
        return self.events[name]

    def attachTextView(self, textView):
        self.textView = textView

    def sendQuitRequest(self, *args):
        self.send(REQ_QUIT)
        sleep(0.1)
        self.stop.set()

    def sendSetNameRequest(self, name):
        self.send(REQ_SET_NAME, [name])
        self.clientName = name

    def sendNewDocumentRequest(self, documentName):
        self.addEventObject('newDocumentSuccessful')
        self.events['requestComplete'].clear()

        self.send(REQ_NEW_DOCUMENT, [documentName, self.clientName])
        self.events['requestComplete'].wait()

        return self.events['newDocumentSuccessful'].is_set()

    def insertTextRequest(self, buffer, iter, text, length):
        log.debug('Inserted at %i: %s', iter.get_offset(), text.decode('utf-8'))
        self.send(REQ_INSERT_TEXT, [str(iter.get_offset()), text])

    def deleteTextRequest(self, buffer, start, end):
        log.debug('Deleted from %i to %i: %s', start.get_offset(), end.get_offset(), buffer.get_text(start, end, False).decode('utf-8'))
        self.send(REQ_DELETE_TEXT, [str(start.get_offset()), str(end.get_offset())])

    def newDocumentRequestHandler(self, params):
        if params[0] == REQ_TRUE:
            self.events['newDocumentSuccessful'].set()
            log.debug('Document successfully created')
        else:
            self.events['newDocumentSuccessful'].clear()
            log.debug('Document already exists')

        self.events['requestComplete'].set()

    def documentListRequestHandler(self, params):
        pass

    def clientListRequestHandler(self, params):
        pass

    def accessListRequestHandler(self, params):
        pass

    def insertTextRequestHandler(self, params):
        pass

    def deleteTextRequestHandler(self, params):
        pass


if __name__ == '__main__':

    logging.setupCustomLogger(LOG_LEVEL)
    log = logging.getCustomLogger()

    app = TextEditorApp()
    app.run()