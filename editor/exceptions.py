from __future__ import absolute_import, division, print_function, unicode_literals
__metaclass__ = type


class DocumentNotFoundError(Exception):
    pass

class PermissionDeniedError(Exception):
    pass