from __future__ import absolute_import, division, print_function, unicode_literals
__metaclass__ = type

import threading
import socket
from collections import deque
from time import sleep

from editor.constants import *
from editor.exceptions import *
import editor.logging as logging


log = logging.getCustomLogger()


class CommunicationThread(threading.Thread):
    """
    Basic duplex communication routines
    """

    def __init__(self):
        threading.Thread.__init__(self)
        self.stop = threading.Event()
        self.requestHandlerTable = {}
        self.incoming = ''
        self.outgoing = deque()
        self.socket = None

    def run(self):
        self.socket.settimeout(0.01)

        while True:
            emptyRun = not self.processOutgoing() and not self.processIncoming()

            if emptyRun:
                if self.stop.isSet():
                    break
                sleep(0.001)

    def processIncoming(self):
        try:
            msg = self.receive()
            if msg == '':
                return False

            self.incoming += msg

            requestEndIndex = self.incoming.find(MSG_END)
            if requestEndIndex >= 0:
                requestString = self.incoming[:requestEndIndex]
                self.incoming = self.incoming[requestEndIndex + 1:]
                log.debug('Received msg: %s', requestString)
                request = requestString.split(MSG_SEPARATOR)

                params = request[1:]
                request = request[0]

                if request in self.requestHandlerTable:
                    self.requestHandlerTable[request](params)
                else:
                    log.error('Unknown request / no handler: %s', requestString)

            return True
        except socket.timeout:
            return False

    def processOutgoing(self):
        if len(self.outgoing):
            try:
                msg = self.outgoing.popleft()
                self.socket.sendall(msg)
                log.debug('Sent msg: %s', msg[:-1].decode('utf-8'))
            except Exception as e:
                log.error('Unhandled exception: %s', str(e))
            return True
        else:
            return False

    def send(self, req, data=None):
        data = [req] if data is None else [req] + data
        msg = MSG_SEPARATOR.join(data) + MSG_END
        self.outgoing.append(msg.encode(encoding='utf-8'))

    def receive(self):
        return self.socket.recv(RECEIVE_BUFSIZE).decode('utf-8')
