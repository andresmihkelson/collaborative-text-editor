from __future__ import absolute_import, division, print_function, unicode_literals
from logging import *

LOGGER_NAME = 'customLogger'

def setupCustomLogger(level):
    formatter = Formatter(fmt=u'%(asctime)s\t\t%(levelname)s\t\t%(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    handler = StreamHandler()
    handler.setFormatter(formatter)
    logger = getLogger(LOGGER_NAME)
    logger.setLevel(level)
    logger.addHandler(handler)

def getCustomLogger():
    return getLogger(LOGGER_NAME)