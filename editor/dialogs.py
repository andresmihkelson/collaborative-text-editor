from __future__ import absolute_import, division, print_function, unicode_literals
__metaclass__ = type

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class TextEntryDialog(Gtk.Dialog):

    def __init__(self, parent, title, question):

        btns = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)
        Gtk.Dialog.__init__(self, title, parent, 0, btns)
        self.set_default_response(Gtk.ResponseType.OK)
        self.set_default_size(350, 100)
        self.set_border_width(10)

        box = self.get_content_area()
        label = Gtk.Label(question)
        label.set_halign(Gtk.Align.START)
        self.entry = Gtk.Entry()
        self.entry.set_activates_default(True)

        box.add(label)
        box.pack_start(self.entry, True, True, 10)

        self.show_all()

    def getValue(self):
        return self.entry.get_text().decode('utf-8')


class DocumentListDialog(Gtk.Dialog):

    def __init__(self, parent):

        btns = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        Gtk.Dialog.__init__(self, 'Open document', parent, 0, btns)

        self.set_default_response(Gtk.ResponseType.OK)
        self.set_default_size(350, 100)
        self.set_border_width(10)

        self.show_all()


class AccessListDialog(Gtk.Dialog):

    def __init__(self, parent):

        btns = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_APPLY, Gtk.ResponseType.APPLY)
        Gtk.Dialog.__init__(self, 'Manage permissions', parent, 0, btns)

        self.set_default_response(Gtk.ResponseType.APPLY)
        self.set_default_size(350, 100)
        self.set_border_width(10)

        self.show_all()
